Pod::Spec.new do |s|
s.name              = 'JanusCallSDK'
s.version           = '1.0'
s.summary           = 'Description of JanusCallSDK Framework.'

s.description      = <<-DESC
A bigger description of JanusCallSDK Framework.
DESC

s.homepage          = 'https://www.innoinstant.com'
s.license           = "MIT"
s.authors           = { 'Jagen K' => 'jagen@innocrux.com' }
s.source            = { :git => "https://bitbucket.org/Innocrux/innoinstant-ios-call-sdk.git", :tag => s.version }
s.vendored_frameworks = 'JanusCallSDK.xcframework'
s.swift_version     = '5.5.2'

s.ios.deployment_target = '12.0'


# Add all the dependencies
s.dependency 'Starscream'

s.pod_target_xcconfig = { 'SWIFT_VERSION' => '5.5.2' }

end