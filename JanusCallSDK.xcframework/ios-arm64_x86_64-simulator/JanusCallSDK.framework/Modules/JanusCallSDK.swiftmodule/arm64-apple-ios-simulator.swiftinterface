// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5.2 (swiftlang-1300.0.47.5 clang-1300.0.29.30)
// swift-module-flags: -target arm64-apple-ios12.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name JanusCallSDK
import Foundation
@_exported import JanusCallSDK
import Starscream
import Swift
import UIKit
import WebRTC
import _Concurrency
public enum SignalingConnectionState {
  case connected([Swift.String : Swift.String])
  case disconnected(Swift.String, Swift.UInt16)
  case error(Swift.Error?)
  case cancelled
}
@_hasMissingDesignatedInitializers public class SignalingClient {
  public var isConnected: Swift.Bool
  @objc deinit
}
extension JanusCallSDK.SignalingClient : Starscream.WebSocketDelegate {
  public func didReceive(event: Starscream.WebSocketEvent, client: Starscream.WebSocket)
}
public class JanusPublisher : Swift.Codable, Swift.CustomDebugStringConvertible, Swift.Hashable {
  public var id: Swift.String
  public var display: Swift.String
  public var videoCodec: Swift.String?
  public var audioCodec: Swift.String?
  required public init(from decoder: Swift.Decoder) throws
  public init?(dict: [Swift.String : Any])
  public var description: Swift.String {
    get
  }
  public var debugDescription: Swift.String {
    get
  }
  public init(id: Swift.String, display: Swift.String)
  public static func == (lhs: JanusCallSDK.JanusPublisher, rhs: JanusCallSDK.JanusPublisher) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
  public var hashValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers public class JanusJoinedRoom : Swift.Codable, Swift.CustomDebugStringConvertible {
  public var id: Swift.String
  public var room: Swift.String
  public var name: Swift.String
  public var privateID: Swift.Int64
  public var publishers: [JanusCallSDK.JanusPublisher]
  required public init(from decoder: Swift.Decoder) throws
  public var debugDescription: Swift.String {
    get
  }
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
}
public class JanusConnection : Swift.Equatable {
  public var handleID: Swift.Int64
  public var publisher: JanusCallSDK.JanusPublisher
  public var rtcClient: JanusCallSDK.WebRTCClient?
  required public init(handleID: Swift.Int64, publisher: JanusCallSDK.JanusPublisher)
  public static func == (lhs: JanusCallSDK.JanusConnection, rhs: JanusCallSDK.JanusConnection) -> Swift.Bool
  public var isLocal: Swift.Bool {
    get
  }
  @objc deinit
}
public protocol AnyEncoder {
  func encode<T>(_ value: T) throws -> Foundation.Data where T : Swift.Encodable
}
extension Foundation.JSONEncoder : JanusCallSDK.AnyEncoder {
}
extension Foundation.PropertyListEncoder : JanusCallSDK.AnyEncoder {
}
extension Swift.Encodable {
  public func encoded(using encoder: JanusCallSDK.AnyEncoder = JSONEncoder()) throws -> Foundation.Data
}
extension Swift.Encoder {
  public func encodeSingleValue<T>(_ value: T) throws where T : Swift.Encodable
  public func encode<T>(_ value: T, for key: Swift.String) throws where T : Swift.Encodable
  public func encode<T, K>(_ value: T, for key: K) throws where T : Swift.Encodable, K : Swift.CodingKey
  public func encode<F>(_ date: Foundation.Date, for key: Swift.String, using formatter: F) throws where F : JanusCallSDK.AnyDateFormatter
  public func encode<K, F>(_ date: Foundation.Date, for key: K, using formatter: F) throws where K : Swift.CodingKey, F : JanusCallSDK.AnyDateFormatter
}
public protocol AnyDecoder {
  func decode<T>(_ type: T.Type, from data: Foundation.Data) throws -> T where T : Swift.Decodable
}
extension Foundation.JSONDecoder : JanusCallSDK.AnyDecoder {
}
extension Foundation.PropertyListDecoder : JanusCallSDK.AnyDecoder {
}
extension Foundation.Data {
  public func decoded<T>(as type: T.Type = T.self, using decoder: JanusCallSDK.AnyDecoder = JSONDecoder()) throws -> T where T : Swift.Decodable
}
extension Swift.Decoder {
  public func decodeSingleValue<T>(as type: T.Type = T.self) throws -> T where T : Swift.Decodable
  public func decode<T>(_ key: Swift.String, as type: T.Type = T.self) throws -> T where T : Swift.Decodable
  public func decode<T, K>(_ key: K, as type: T.Type = T.self) throws -> T where T : Swift.Decodable, K : Swift.CodingKey
  public func decodeIfPresent<T>(_ key: Swift.String, as type: T.Type = T.self) throws -> T? where T : Swift.Decodable
  public func decodeIfPresent<T, K>(_ key: K, as type: T.Type = T.self) throws -> T? where T : Swift.Decodable, K : Swift.CodingKey
  public func decode<F>(_ key: Swift.String, using formatter: F) throws -> Foundation.Date where F : JanusCallSDK.AnyDateFormatter
  public func decode<K, F>(_ key: K, using formatter: F) throws -> Foundation.Date where K : Swift.CodingKey, F : JanusCallSDK.AnyDateFormatter
  public func decode<T>(_ keys: [Swift.CodingKey], as type: T.Type = T.self) throws -> T where T : Swift.Decodable
  public func decode<T>(_ keys: [Swift.String], as type: T.Type = T.self) throws -> T where T : Swift.Decodable
  public func decode<T>(keyPath: Swift.String, as type: T.Type = T.self) throws -> T where T : Swift.Decodable
}
public protocol AnyDateFormatter {
  func date(from string: Swift.String) -> Foundation.Date?
  func string(from date: Foundation.Date) -> Swift.String
}
extension Foundation.DateFormatter : JanusCallSDK.AnyDateFormatter {
}
@available(iOS 10.0, macOS 10.12, tvOS 10.0, *)
extension Foundation.ISO8601DateFormatter : JanusCallSDK.AnyDateFormatter {
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers final public class WebRTCClient : ObjectiveC.NSObject {
  @objc deinit
}
extension JanusCallSDK.WebRTCClient {
  @available(iOSApplicationExtension, unavailable)
  final public func switchCamera(renderer: WebRTC.RTCMTLVideoView)
}
extension JanusCallSDK.WebRTCClient : WebRTC.RTCPeerConnectionDelegate {
  @objc final public func peerConnection(_ peerConnection: WebRTC.RTCPeerConnection, didChange stateChanged: WebRTC.RTCSignalingState)
  @objc final public func peerConnection(_ peerConnection: WebRTC.RTCPeerConnection, didAdd stream: WebRTC.RTCMediaStream)
  @objc final public func peerConnection(_ peerConnection: WebRTC.RTCPeerConnection, didRemove stream: WebRTC.RTCMediaStream)
  @objc final public func peerConnectionShouldNegotiate(_ peerConnection: WebRTC.RTCPeerConnection)
  @objc final public func peerConnection(_ peerConnection: WebRTC.RTCPeerConnection, didChange newState: WebRTC.RTCIceConnectionState)
  @objc final public func peerConnection(_ peerConnection: WebRTC.RTCPeerConnection, didChange newState: WebRTC.RTCIceGatheringState)
  @objc final public func peerConnection(_ peerConnection: WebRTC.RTCPeerConnection, didGenerate candidate: WebRTC.RTCIceCandidate)
  @objc final public func peerConnection(_ peerConnection: WebRTC.RTCPeerConnection, didRemove candidates: [WebRTC.RTCIceCandidate])
  @objc final public func peerConnection(_ peerConnection: WebRTC.RTCPeerConnection, didOpen dataChannel: WebRTC.RTCDataChannel)
}
extension JanusCallSDK.WebRTCClient {
  final public func hideVideo()
  final public func showVideo()
}
extension JanusCallSDK.WebRTCClient {
  final public func muteAudio()
  final public func unmuteAudio()
  final public func speakerOff()
  final public func speakerOn()
}
public protocol JanusRoomManagerNotifying {
  func getJanusPublisherId() -> Swift.String
  func roomCreated()
}
public enum JanusRoomState {
  case `default`, joined, publishingSubscribing, subscribingOnly, left, endByRemote
  public var isStreaming: Swift.Bool {
    get
  }
  public static func == (a: JanusCallSDK.JanusRoomState, b: JanusCallSDK.JanusRoomState) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension JanusCallSDK.JanusRoomManager {
  public static let signalingStateChangeNote: Foundation.Notification.Name
  public static let roomStateChangeNote: Foundation.Notification.Name
  public static let rtcClientStateChangeNote: Foundation.Notification.Name
  public static let publisherDidLeaveRoomNote: Foundation.Notification.Name
  public static let publisherDidJoinRoomNote: Foundation.Notification.Name
  public static let didReceiveErrorResponse: Foundation.Notification.Name
  public static let externalSampleCapturerDidCreateNote: Foundation.Notification.Name
}
@_hasMissingDesignatedInitializers final public class JanusRoomManager {
  public static let shared: JanusCallSDK.JanusRoomManager
  final public var delegate: JanusCallSDK.JanusRoomManagerNotifying?
  final public var room: Swift.String
  final public var currentRoom: JanusCallSDK.JanusJoinedRoom?
  final public var connections: [JanusCallSDK.JanusConnection]
  final public var signalingClient: JanusCallSDK.SignalingClient
  final public var roomState: JanusCallSDK.JanusRoomState
  final public var webRTCIceServers: [JanusCallSDK.Config.WebrtcCredential]
  final public var signalServerURL: Swift.String
  final public var maxParticipants: Swift.Int
  final public func reset()
  @objc deinit
}
extension JanusCallSDK.JanusRoomManager {
  final public var localConnection: JanusCallSDK.JanusConnection? {
    get
  }
}
extension JanusCallSDK.JanusRoomManager {
  final public func connect()
  final public func disconnect()
  final public func joinRoom(room: Swift.String)
  final public func leaveCurrentRoom()
}
extension JanusCallSDK.JanusRoomManager {
  final public func createLocalJanusConnection() -> JanusCallSDK.JanusConnection
}
extension WebRTC.RTCIceConnectionState : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
extension WebRTC.RTCSignalingState : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
extension WebRTC.RTCIceGatheringState : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
extension WebRTC.RTCDataChannelState : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
public struct Config {
  public struct WebrtcCredential {
    public let auth: Swift.Bool?
    public let host: Swift.String?, password: Swift.String?, type: Swift.String?, username: Swift.String?
    public init(auth: Swift.Bool, host: Swift.String, password: Swift.String, type: Swift.String, username: Swift.String)
  }
}
extension JanusCallSDK.JanusRoomState : Swift.Equatable {}
extension JanusCallSDK.JanusRoomState : Swift.Hashable {}
